#include <gtk/gtk.h>
#include <time.h>

#include "guimain.h"

/*
 * Removes a hbox widget from gui.
 * 
 * This function is called when a checkbox is toggled.
 * Get's the parent widget of the hbox passed in, 
 * then removes the hbox from the parent.
 * 
 * Parameters:
 * 		- GtkWidget *check, the check button toggled.
 * 		- GtkWidget *hbox, the widget to be removed.
 */
void on_check_toggled(GtkWidget *check, GtkWidget *hbox)
{
	// get hbox's parent widget
	GtkWidget *parent = gtk_widget_get_parent(hbox);
	
	// remove hbox from parent
	gtk_container_remove
	(
		GTK_CONTAINER(parent), 
		hbox
	);
}

/*
 * Gets the current date and time
 * and formats it.
 * 
 * returns: char* date, A formated string with the current date
 */
char* get_date()
{
	// Gets current time
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	
	// Allocates storage
	char *date = (char*)malloc(15 * sizeof(char));

	// Formats Date String
	sprintf
	(
		date, 
		"%d/%d/%d - %d:%d:%d", 
		tm.tm_mday, 
		tm.tm_mon + 1, 
		tm.tm_year + 1900, 
		tm.tm_hour, 
		tm.tm_min, 
		tm.tm_sec
	);
	
	return date;
}

/*
 * Creates the widget to be added when a task is added.
 * 
 * Sets up the layout and style of a todo item, 
 * and adds the user submitted text, 
 * and the datetime of creation.
 * 
 * Parameters:
 * 		- const gchar *text, the text to be added.
 * 
 * returns GtkWidget *hbox, the parent of the other widgets.
 */
GtkWidget* layout_of_todo_item(const gchar *text)
{
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 12);
	GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);
	GtkWidget *timeLabel = gtk_label_new(get_date());
	GtkWidget *content 	= gtk_label_new(text);
	GtkWidget *check = gtk_check_button_new();
	
	
	// content label properties
	gtk_label_set_line_wrap(GTK_LABEL(content), TRUE);
	gtk_label_set_line_wrap_mode
	(
		GTK_LABEL(content), 
		PANGO_WRAP_WORD_CHAR
	);
	
	// set time label alignment
	gtk_widget_set_halign(timeLabel, GTK_ALIGN_START);
	
	// Expand vbox horizontally
	gtk_widget_set_hexpand(vbox, TRUE);
	
	// Padding
	gtk_widget_set_margin_start(hbox, 12);
	gtk_widget_set_margin_end(hbox, 12);
	gtk_widget_set_margin_top(hbox, 12);
	gtk_widget_set_margin_bottom(hbox, 12);
	
	gtk_widget_set_margin_start(content, 12);
	gtk_widget_set_margin_end(content, 12);
	gtk_widget_set_margin_top(content, 12);
	gtk_widget_set_margin_bottom(content, 12);

	// Add widgets to vbox	
	gtk_container_add(GTK_CONTAINER(vbox), timeLabel);
	gtk_container_add(GTK_CONTAINER(vbox), content);
	
	// Add widgets to hbox
	gtk_container_add(GTK_CONTAINER(hbox), vbox);
	gtk_container_add(GTK_CONTAINER(hbox), check);
	
	// checkbox signal
	g_signal_connect
			(check, "toggled", G_CALLBACK(on_check_toggled), hbox);
		
	return hbox;
}

/*
 * Adds a todo item on button clicked.
 * 
 * Creates a new box row, and gets the text from the entry.
 * Checks that the entry wasn't empty, if not empty add todo 
 * item and reset entry.
 * 
 * Properties:
 * 		- GtkWidget *button, the button that was clicked.
 * 		- GuiMain *gui, the gui to add the item to.
 */
void on_btnAdd_clicked(GtkWidget *button, GuiMain *gui)
{
	// create row
	GtkWidget *row = gtk_list_box_row_new();
	const gchar *content = 
			gtk_entry_get_text
			(
				GTK_ENTRY(gui->entry)
			);
	
	if(content != NULL && content[0] != '\0')
	{	
		// row layout
		gtk_container_add
		(
			GTK_CONTAINER(row), 
			layout_of_todo_item(content)
		);
		
		gtk_list_box_row_changed(GTK_LIST_BOX_ROW(row));
		
		// add to row
		gtk_list_box_insert
		(
			GTK_LIST_BOX(gui->listbox), 
			row,
			-1
		);
		
		// reset entry
		gtk_entry_set_text(GTK_ENTRY(gui->entry), "");
		gtk_widget_show_all(gui->window);
	}
}

/*
 * Creates the Main GUI's window.
 * 
 * Creates all widgets and instantiates the
 * GuiMain struct variables. Sets up properties for widgets,
 * adds widgets to containers and connect signals.
 * 
 * Properties:
 * 		- GuiMain *gui, The GUI to be created
 */
void createWindow(GuiMain *gui)
{
	gui->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	GtkWidget *boxMain = gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);
	GtkWidget *boxBtns = 
			gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 12);
	gui->entry = gtk_entry_new();
	gui->btnAdd = gtk_button_new_with_label("   Add Item   ");
	GtkWidget *scroll = gtk_scrolled_window_new(NULL, NULL);
	gui->listbox = gtk_list_box_new();	
	
	// Window properties
	gtk_window_set_title(GTK_WINDOW(gui->window), "To Do");
	gtk_window_set_resizable(GTK_WINDOW(gui->window), 1);
	gtk_window_set_default_size(GTK_WINDOW(gui->window), 300, 350);
	gtk_window_set_position
	(
		GTK_WINDOW(gui->window), 
		GTK_WIN_POS_CENTER
	);
	
	
	// Other widget properties
	gtk_widget_set_hexpand(gui->entry, TRUE);
	gtk_widget_set_vexpand(scroll, TRUE);
	
	// Padding
	gtk_widget_set_margin_start(boxMain, 12);
	gtk_widget_set_margin_end(boxMain, 12);
	gtk_widget_set_margin_top(boxMain, 12);
	gtk_widget_set_margin_bottom(boxMain, 12);
	gtk_box_set_spacing(GTK_BOX(boxMain), 12);
		
	
	// Adding containers
	gtk_container_add(GTK_CONTAINER(gui->window), boxMain);
	gtk_container_add(GTK_CONTAINER(boxMain), boxBtns);
	gtk_container_add(GTK_CONTAINER(boxBtns), gui->entry);
	gtk_container_add(GTK_CONTAINER(boxBtns), gui->btnAdd);
	gtk_container_add(GTK_CONTAINER(boxMain), scroll);
	gtk_container_add
			(GTK_CONTAINER(scroll), GTK_WIDGET(gui->listbox));
	
	// Signals
	g_signal_connect
	(
		gui->window, 
		"destroy", 
		G_CALLBACK(gtk_main_quit), 
		NULL
	); 
	
	g_signal_connect
	(
		gui->btnAdd, 
		"clicked", 
		G_CALLBACK(on_btnAdd_clicked), 
		gui
	);
	
	gtk_widget_show_all(gui->window);
}
