#include <gtk/gtk.h>
#include <stdio.h>

#include "guimain.h"

int main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
	
	GuiMain gui;
	createWindow(&gui);
	
	gtk_main();
	return 0;
}
