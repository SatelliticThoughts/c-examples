# To Do

A very simple to do application, to practice C and Gtk.

## Setting up

After cloning/downloading the project run

```
make # to build the project
```

## Usage

Write in the entry field and click the Add Item button,
to add an item to the list.

Toggle the checkbutton to remove it.

![](img/application.png)

## License
[MIT](https://choosealicense.com/licenses/mit/)
