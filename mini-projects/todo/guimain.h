#ifndef GUIMAIN_H
#define GUIMAIN_H

typedef struct GuiMain {
	GtkWidget *window;
	GtkWidget *btnAdd;
	GtkWidget *entry;
	GtkWidget *listbox;
} GuiMain;

void createWindow(GuiMain *gui);

#endif
