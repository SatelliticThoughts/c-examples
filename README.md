# c-examples

C-examples contains code to demonstrate various things in the C programming language.

## Set up

After cloning or downloading, most programs should be a single file that can be compiled with gcc and executed, unless otherwise stated.

```
gcc -o 'filename' 'filename.c' # compile file
./'filename' # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
