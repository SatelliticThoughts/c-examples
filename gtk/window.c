#include <gtk/gtk.h>
#include <stdio.h>


/* 
 * Basic gtk window example
 *
 * Compiling: gcc -g -Wall -o file.exe file.c `pkg-config --cflags gtk+-3.0 --libs gtk+-3.0`
 *
 */


int main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);

	// Window Settings
	GtkWidget *win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(win), "Window Title!");
	gtk_window_set_resizable(GTK_WINDOW(win), 1);
	gtk_window_set_default_size(GTK_WINDOW(win), 500, 300);
	gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_CENTER);

	GtkImage *image = GTK_IMAGE( 
		gtk_image_new_from_file(
			"image.png"));

	GdkPixbuf *icon = GDK_PIXBUF(gtk_image_get_pixbuf(image));

	gtk_window_set_default_icon(icon);
	
	// signals
	g_signal_connect(win, "destroy", G_CALLBACK(gtk_main_quit), NULL); 
	
	gtk_widget_show_all(win);
	
	gtk_main();
	return 0;
}

