#include <gtk/gtk.h>
#include <stdio.h>

/* 
 * Basic gtk button example
 *
 * Compiling: gcc -g -Wall -o file.exe file.c `pkg-config --cflags gtk+-3.0 --libs gtk+-3.0`
 *
 */

void on_btn_ok_clicked(GtkWidget *widget, gpointer *data)
{
	g_print("Ok clicked\n");
}


int main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);

	GtkWidget *win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(win), 500, 300);

	GtkWidget *btn_ok = gtk_button_new_with_label("Ok");

	// signals
	g_signal_connect(win, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	g_signal_connect(btn_ok, "clicked", G_CALLBACK(on_btn_ok_clicked), NULL);

	gtk_container_add(GTK_CONTAINER(win), btn_ok);
	gtk_widget_show_all(win);
	gtk_main();
	return 0;
}

