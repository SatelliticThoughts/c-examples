#include <stdio.h>
#include <gtk/gtk.h>


void on_btn_open_clicked(GtkWidget *button, GtkWidget *textview) 
{
	GtkTextBuffer *buffer = gtk_text_buffer_new(NULL);
	GtkWidget *dialog;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint res;

	// set up GtkDialog
	dialog = 
			gtk_file_chooser_dialog_new 
			(
				"Open File",
				NULL,
				action,
				("_Cancel"),
				GTK_RESPONSE_CANCEL,
				("_Open"),
				GTK_RESPONSE_ACCEPT,
				NULL
			);

	// run GtkDialog
	res = gtk_dialog_run (GTK_DIALOG (dialog));
	
	// If user selects a file
	if (res == GTK_RESPONSE_ACCEPT) 
	{
    	char *filename;
    	
    	// set file to user's choice
    	GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
    	filename = gtk_file_chooser_get_filename (chooser);
    	
    	// add file contents to buffer
		// buffer = set_buffer_as_file(filename);
		gchar *text;
	
		if(g_file_get_contents(filename, &text, NULL, NULL)) 
		{
			gtk_text_buffer_set_text(buffer, text, (int)strlen(text));
		}
    	g_free (filename);
  	}

	// remove GtkDialog
	gtk_widget_destroy (dialog);
	
	// set textviews buffer to the buffer with the file contents
	gtk_text_view_set_buffer(GTK_TEXT_VIEW(textview), buffer);
}

void on_btn_save_clicked(GtkWidget *button, GtkWidget *textview) 
{
	GtkTextBuffer *buffer = 
			gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	GtkTextIter iterStart;
	GtkTextIter iterEnd;
	
	gtk_text_buffer_get_start_iter(buffer, &iterStart);
	gtk_text_buffer_get_end_iter(buffer, &iterEnd);
	gchar *text = 
			gtk_text_buffer_get_text(buffer, &iterStart, &iterEnd, TRUE);
	
	GtkWidget *dialog;

	dialog = 
			gtk_file_chooser_dialog_new 
			(
				"Save File",
				NULL,
				GTK_FILE_CHOOSER_ACTION_SAVE,
				("_Cancel"), GTK_RESPONSE_CANCEL,
				("_Save"), GTK_RESPONSE_ACCEPT,
				NULL
			);
			
	gtk_file_chooser_set_do_overwrite_confirmation 
	(
		GTK_FILE_CHOOSER (dialog), 
		TRUE
	);

	gtk_file_chooser_set_current_name 
	(
		GTK_FILE_CHOOSER (dialog), 
		"Untitled document"
	);
	
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	  {
		char *filename;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		if(g_file_set_contents(filename, text, (int)strlen(text), NULL)) 
		{
		} else{}
		g_free (filename);
	  }

	gtk_widget_destroy (dialog);
}

int main(int argc, char *argv[]) 
{
	gtk_init(&argc, &argv);

	// Window
	GtkWidget *mainWin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(mainWin), "Text Editor");
	gtk_window_set_resizable(GTK_WINDOW(mainWin), 1);
	gtk_window_set_default_size(GTK_WINDOW(mainWin), 500, 300);
	gtk_window_set_position(GTK_WINDOW(mainWin), GTK_WIN_POS_CENTER);

	// icon
	GtkImage *image = 
			GTK_IMAGE
			(
				gtk_image_new_from_file
			        (
					"image.png"
				)	
			);

	GdkPixbuf *icon = 
			GDK_PIXBUF
			(
				gtk_image_get_pixbuf
				(
					image
				)
			);
	
	gtk_window_set_default_icon(icon);
	
	// boxMain
	GtkWidget *boxMain = gtk_box_new(GTK_ORIENTATION_VERTICAL, 6);
	gtk_container_add(GTK_CONTAINER(mainWin), boxMain);
	
	// boxBtns
	GtkWidget *boxBtns = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_container_add(GTK_CONTAINER(boxMain), boxBtns);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(boxBtns), GTK_BUTTONBOX_START);
	
	// buttons
	GtkWidget *btnOpen = gtk_button_new_with_label("Open");
	gtk_container_add(GTK_CONTAINER(boxBtns), btnOpen);

	GtkWidget *btnSave = gtk_button_new_with_label("Save");
	gtk_container_add(GTK_CONTAINER(boxBtns), btnSave);


	// textview
	GtkWidget *scroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(boxMain), scroll);
	gtk_widget_set_vexpand(scroll, TRUE);

	GtkWidget *txtview = gtk_text_view_new();
	gtk_container_add(GTK_CONTAINER(scroll), txtview);
	
	// padding
	gtk_widget_set_margin_start(boxMain, 12);
	gtk_widget_set_margin_end(boxMain, 12);
	gtk_widget_set_margin_top(boxMain, 12);
	gtk_widget_set_margin_bottom(boxMain, 12);
	
	gtk_box_set_spacing(GTK_BOX(boxMain), 12);
	// signals
	g_signal_connect
	(
		mainWin, 
		"destroy", 
		G_CALLBACK(gtk_main_quit), 
		NULL
	);
	
	g_signal_connect
	(
		btnOpen, 
		"clicked", 
		G_CALLBACK(on_btn_open_clicked), 
		txtview
	);
	
	g_signal_connect
	(
		btnSave, 
		"clicked", 
		G_CALLBACK(on_btn_save_clicked), 
		txtview
	);

	gtk_widget_show_all(mainWin);

	gtk_main();

	return 0;
}
