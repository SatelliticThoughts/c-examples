#include <stdio.h>

int main(int argc, char** argv)
{
	int running = 1;
	int num = 0;

	// while loop
	while(running)
	{
		num += 1;
		if(num > 5)
		{
			running = 0;
		}
	}

	// for loop
	for(int i = 1; i < 13; i++)
	{
		printf("12 / %d = %d\n", i, 12 / i);
	}

	return 0;
}

