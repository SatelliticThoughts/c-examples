#include <stdio.h>

int main(int argc, char **argv)
{
	// Basic if statement.
	if(1)
	{
		printf("This will show\n");
	}

	// Basic if/else statement.
	if(0)
	{
		printf("This will NOT show\n");
	}
	else
	{
		printf("This will show\n");
	}

	// Basic if/else if/else statement.
	if(0)
	{
		printf("This will NOT show\n");
	}
	else if(1)
	{
		printf("This will show\n");
	}
	else
	{
		printf("This will NOT show\n");
	}

	// &&, and
	if(1 && 1)
	{
		printf("This will show\n");
	}

	if(1 && 0)
	{
		printf("This will NOT show\n");
	}

	if(0 && 0)
	{
		printf("This will NOT show\n");
	}

	// ||, or
	if(1 || 1)
	{
		printf("This will show\n");
	}

	if(1 || 0)
	{
		printf("This will show\n");
	}

	if(0 || 0)
	{
		printf("This will NOT show\n");
	}

	int x = 3;
	int y = 7;

	// Equals
	if(x == y)
	{
		printf("If x equals y this will show\n");
	}

	// Not Equal
	if(x != y)
	{
		printf("If x does not equal y this will show\n");
	}

	// Greater Than
	if(x > y)
	{
		printf("If x is bigger than y this will show\n");
	}

	// Less Than
	if(x < y)
	{
		printf("If x is less than y this will show\n");
	}
	return 0;
}

