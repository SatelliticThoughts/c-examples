#include <stdio.h>

int main(int argv, char **argc)
{
	// Integers
	int x = 35;
	int y = 7;

	printf("Integer Calculations, x = %d, y = %d\n", x, y);

	printf("x + y = %d\n", x + y); // Addition
	printf("x - y = %d\n", x - y); // Subtraction
	printf("x * y = %d\n", x * y); // Multiplication
	printf("x / y = %d\n", x / y); // Division
	printf("x mod y = %d\n", x % y); // Modulus

	// Floating point numbers
	float a = 2.312f;
	float b = 1.1f;

	printf("\nFloating point Calculations, a = %f, b = %f\n", a, b);
	printf("a + b = %f\n", a + b); // Addition
	printf("a - b = %f\n", a - b); // Subtraction
	printf("a * b = %f\n", a * b); // Multiplication
	printf("a / b = %f\n", a / b); // Division

	// Strings
	printf("\nString Manipulation\n");

	char hello[] = "hello";
	printf("%s\n", hello);

	printf("%.*s\n", 3, hello + 1); // substring

	// Arrays
	int numbers[3];
	int length = sizeof(numbers)/sizeof(numbers[0]);
	for(int i = 0; i < length; i++)
	{
		numbers[i] = i * 8;
	}

	printf("%d\n", numbers[1]);

	return 0;
}

