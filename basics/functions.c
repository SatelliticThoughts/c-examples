#include <stdio.h>

// Function without parameters and return statement.
void sayHello()
{
	printf("Hello\n");
}

// Function with return statement.
char* getWord()
{
	return "dog";
}

// Function with single parameter.
void doubleNumber(int x)
{
	printf("%d\n", x * 2);
}

// Function with multiple parameters and return statement.
int addNumbers(int x, int y)
{
	return x + y;
}

int main(int argc, char **argv)
{
	sayHello();
	printf("%s\n", getWord());
	doubleNumber(12);
	printf("%d\n", addNumbers(3, 5));

	return 0;
}

