#include <stdio.h>
#include <string.h>

struct Person 
{
	char name[25];
	int age;
};

int main(int argc, char** argv)
{
	struct Person p;
	strcpy(p.name, "luke");
	p.age = 50;

	printf("Name: %s Age: %d\n", p.name, p.age);

	return 0;
}

