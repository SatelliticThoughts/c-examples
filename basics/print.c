#include <stdio.h>

int main(int argc, char **argv)
{
	// Hello World example
	printf("Hello World!\n");

	// Printing variable example
	char text[] = "This is a test\n";
	printf(text);

	// Printing multiple variables
	char name[] = "luke";
	int age = 1000;
	printf("My name is %s and i am %d years old\n", name, age);

	return 0;
}

