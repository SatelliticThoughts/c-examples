/*
 * Problem:
 * 		If we list all the natural numbers below 10 
 * 		that are multiples of 3 or 5, we get 3, 5, 6 and 9. 
 * 		The sum of these multiples is 23.
 *
 * 		Find the sum of all the multiples of 3 or 5 below 1000.
 *
 * Origin: Project Euler
 * Link: https://projecteuler.net/problem=1
 *
 */

#include <stdio.h>


int is_multiple(int x)
{
	return (x % 3 == 0 || x % 5 == 0);
}

int sum(int n) 
{
	int sum = 0;
	for(int i = 1; i < n; i++)
	{
		sum += i * is_multiple(i);
	}
	return sum;
}

int main() 
{
	printf("Answer for 10: %d\n", sum(10));
	printf("Answer for 1000: %d\n", sum(1000));
	return 0;
}
