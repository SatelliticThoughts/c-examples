/*
 * Problem:
 * 		Each new term in the Fibonacci sequence is generated 
 * 		by adding the previous two terms. By starting with 1 and 2, 
 * 		the first 10 terms will be:
 * 				1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 *
 * 		By considering the terms in the Fibonacci sequence whose values 
 * 		do not exceed four million, find the sum of the even-valued terms.
 *
 * Origin: Project Euler
 * Link: https://projecteuler.net/problem=2
 *
 */


#include <stdio.h>


void swap(int* x, int* y)
{
	*x = *x + *y;
	*y = *x - *y;
}

int sum(int n)
{
	int sum = 1;
	
	int prev = 1;
	int curr = 1;
	
	while(curr < n)
	{
		sum += curr * (curr % 2);
		swap(&curr, &prev);	
	}

	return sum;
}

int main() 
{
	printf("Answer: %d\n", sum(4000000));
	
	return 0;
}

